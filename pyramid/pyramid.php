<?php
/*

    *
   ***
  *****
 *******
*********

Write a script to output this pyramid on console (with leading spaces)

*/

function pyramid($height)
{
    echo '<pre>';  
    for($i = 1; $i <= $height; $i++){
        $l = $height - $i;
        while($l > 0 ){
            echo '&nbsp;';
            $l--;
        }

        $v = $i + $i - 1;
        while($v > 0 ){
            echo '*';
            $v--;
        }
        
        echo '<br />';
    }
    echo '</pre>';
}

pyramid(5);
