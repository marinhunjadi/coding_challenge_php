<?php
namespace hr;

/**
 *
 * Task: Given is the following FizzBuzz application which counts from 1 to 100 and outputs either the corresponding
 * number or if one of the following rules apply output the corresponding text.
 * Rules:
 *  - dividable by 3 without a remainder -> Fizz
 *  - dividable by 5 without a remainder -> Buzz
 *  - dividable by 3 or 5 without a remainder -> FizzBuzz
 *
 * Please refactor this code so that it can be extended in the future with other rules, such as
 * "if it is dividable by 7 without a remainder output Bar"
 * "if multiplied by 10 is larger than 100 output Foo"
 *
 */

class Operator
{
    const Addition = 0;
    const Subtraction = 1;
    const Multiplication = 2;
    const Division = 3;
    const Modulus = 4;
    const Exponentiation = 5;
}

class Rule
{
    protected int $ruleNumber;
    protected int $ruleOperator;
    protected string $ruleMessage;

    public function __construct(int $ruleNumber, int $ruleOperator, string $ruleMessage) {
        $this->ruleNumber = $ruleNumber;
        $this->ruleOperator = $ruleOperator;
        $this->ruleMessage = $ruleMessage;
    }

    public function getRuleNumber(){
        return $this->ruleNumber;
    }

    public function getRuleOperator(){
        return $this->ruleOperator;
    }

    public function getRuleMessage(){
        return $this->ruleMessage;
    }
}

class FizzBuzzEngine
{
    public function run(int $limit, array $rules)
    {
        for ($i = 1; $i <= $limit; $i++) {
            $output = '';

            foreach ($rules as $rule) {
                switch ($rule->getRuleOperator()) {
                    case Operator::Addition:
                        //TODO
                        $output .= $rule->getRuleMessage();
                        break;
                    case Operator::Subtraction:
                        //TODO
                        $output .= $rule->getRuleMessage();
                        break;
                    case Operator::Multiplication:
                        if ($i * $rule->getRuleNumber() > $limit) {
                            $output .= $rule->getRuleMessage();
                        } 
                        break;
                    case Operator::Division:
                        //TODO
                        $output .= $rule->getRuleMessage();
                        break; 
                    case Operator::Modulus:
                        if ($i % $rule->getRuleNumber() == 0) {
                            $output .= $rule->getRuleMessage();
                        }                        
                        break;
                    case Operator::Exponentiation:
                        //TODO
                        $output .= $rule->getRuleMessage();
                        break;
                    default:
                        break;                 
                  }
            }
            if (empty($output)) {
                $output = 'None';
            }
            echo sprintf('%d: %s', $i, $output . PHP_EOL);
        }
    }
}

$engine = new FizzBuzzEngine();
$rules = [new Rule(3, Operator::Modulus, 'Fizz'), new Rule(5, Operator::Modulus, 'Buzz'), new Rule(7, Operator::Modulus, 'Bar'), new Rule(10, Operator::Multiplication, 'Foo')];

$engine->run(100, $rules);